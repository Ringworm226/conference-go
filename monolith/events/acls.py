from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_image(city, state):
    headers = {"Authorization": PEXELS_API_KEY}

    res = requests.get(
        f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1",
        headers=headers,
    )

    return res.json()["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    headers = {
        "Authorization": OPEN_WEATHER_API_KEY,
    }

    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    lat = res.json([0]["lat"])
    lon = res.json([0]["lon"])
    print(lat, lon)

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    weather_description = res.json()["weather"][0]["description"]
    current_temp = res.json()["main"]["temp"]

    temp = {"temperature": current_temp, "weather": weather_description}
    return temp
